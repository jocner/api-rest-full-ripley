const express = require('express');
const router = express.Router();
const axios = require('axios');
const { transferencia } = require('../modulos');
const transfHandlers = transferencia({ axios });


// router.post('/', 
//    transfHandlers.busqueda
// );

router.get('/', 
   transfHandlers.busqueda
);

module.exports = router;