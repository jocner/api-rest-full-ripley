const express = require('express');
const router = express.Router();
const { transaciones } = require('../modulos');
const transfHandlers = transaciones();


router.post('/', 
   transfHandlers.transferir
);

module.exports = router;